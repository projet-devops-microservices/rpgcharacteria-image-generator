import express from "express";
import {Configuration,OpenAIApi} from "openai";
import * as fs from "fs";
import 'dotenv/config';
import cors from "cors";
import bp from "body-parser";

const config = new Configuration({
    apiKey: process.env.API_KEY,
})

const openai = new OpenAIApi(config);
const app = express();


app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));
app.use(cors({ origin: '*/*' }));


app.listen(process.env.PORT,()=>{
    console.log('server listening on port '+process.env.PORT);
    try {
        console.log('connection succeded');
    } catch (error) {
        console.log('connection error');
        console.log(error);
    }
})


app.post("/generate-image",(req,res)=>{

    openai.createImage({
        model: "dall-e-2",
        prompt: `un ${req.body.personnage.espece} qui travaille en tant que ${req.body.personnage.metier} et a une classe de ${req.body.personnage.class}`,
        n: 1,
        size: "256x256",
    }).then((data)=>{
        res.json({ imageUrl: data.data.data[0].url });
    }).catch((error) => {
        console.error("Erreur lors de la génération de l'image :", error);
        res.status(500).send("Erreur interne du serveur");
    });
})

